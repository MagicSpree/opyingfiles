package com.company;

import com.company.io.IO;
import com.company.model.UtilFile;
import com.company.nio.NIO;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Main {

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        File firstFile = new File("/tmp/testProgram/file1");
        File secondFile = new File("/tmp/testProgram/file2");
        File destFile = new File("/tmp/testProgram/destfile2");
        UtilFile io = new IO(firstFile, secondFile, destFile);
        UtilFile nio = new NIO(firstFile, secondFile, destFile);

//        io.copyWithoutThread();
//        io.copyWithThread();
//
//        nio.copyWithoutThread();
//        nio.copyWithThread();

    }
}
