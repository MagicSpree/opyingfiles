package com.company.io;

import java.io.*;

public class IOThread implements Runnable {

    File firstFile;
    File destFile;
    IO io;

    public IOThread(File firstFile, File secondFile, File destFile) {
        this.firstFile = firstFile;
        this.destFile = destFile;
        io = new IO(firstFile, secondFile, destFile);
    }

    @Override
    public void run() {
        try {
            io.consistentCopyingIO(firstFile, destFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
