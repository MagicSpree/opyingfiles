package com.company.io;

import com.company.model.UtilFile;

import java.io.*;

public class IO extends UtilFile {

    FileInputStream inputFile;

    public IO(File firstFile, File secondFile, File destFile) {
        super(firstFile, secondFile, destFile);
    }


    @Override
    public void copyWithoutThread() {
        try {
            consistentCopyingIO(firstFile, destFile);
            consistentCopyingIO(secondFile, destFile);
            printSuccess();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void copyWithThread() {
        startThreads(firstFile, destFile);
        startThreads(secondFile, destFile);
        printSuccess();
    }

    private void startThreads(File sourceFile, File destFile) {
        Thread thread = new Thread(new IOThread(sourceFile,secondFile,destFile));
        thread.start();
    }

    public void consistentCopyingIO(File sourceFile, File destFile) throws IOException {
        BufferedWriter outputFile = new BufferedWriter(new FileWriter(destFile, true));
        inputFile = new FileInputStream(sourceFile);
        while (inputFile.available() > 0) {
            int data = inputFile.read();
            synchronized (this) {
                outputFile.write(data);
            }
        }
        outputFile.close();
    }
}
