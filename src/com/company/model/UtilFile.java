package com.company.model;

import java.io.File;

public abstract class UtilFile {
    protected File firstFile;
    protected File secondFile;
    protected File destFile;

    public UtilFile(File firstFile, File secondFile, File destFile) {
        this.firstFile = firstFile;
        this.secondFile = secondFile;
        this.destFile = destFile;
    }

    public abstract void copyWithoutThread();

    public abstract void copyWithThread();

    public void printSuccess() {
        System.out.println("Копирование прошло успешно");
    }
}
