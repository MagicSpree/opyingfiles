package com.company.nio;

import com.company.model.UtilFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class NIO extends UtilFile {

    public NIO(File firstFile, File secondFile, File destFile) {
        super(firstFile, secondFile, destFile);
    }

    @Override
    public void copyWithoutThread() {
        try {
            copyFileUsingChannel(firstFile);
            copyFileUsingChannel(secondFile);
            printSuccess();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void copyWithThread() {
        try {
            readAsynchronousNIO();
            printSuccess();
        } catch (IOException | ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void readAsynchronousNIO() throws IOException, ExecutionException, InterruptedException {
        AsynchronousFileChannel channelRead = AsynchronousFileChannel.open(firstFile.toPath());
        AsynchronousFileChannel channelWrite = AsynchronousFileChannel.open(destFile.toPath(), StandardOpenOption.WRITE);
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        long position = 0;

        do {

            Future<Integer> resultRead = channelRead.read(buffer, position);
            if (resultRead == null) {
                break;
            }

            int blockRead = resultRead.get();
            if (blockRead < 0) {
                break;
            }

            buffer.flip();
            Future<Integer> resultWrite = channelWrite.write(buffer, position);
            if (resultWrite != null) {
                resultWrite.get();
            }

            buffer.clear();
            position += blockRead;
        } while (true);

        channelRead.close();
        channelWrite.close();
    }


    private void copyFileUsingChannel(File sourceFile) throws IOException {
        try (FileChannel sourceChannel = new FileInputStream(sourceFile).getChannel();
             FileChannel destChannel = new FileOutputStream(destFile).getChannel()) {
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        }
    }
}
